/*
Sparrow, Login Javascript
© Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

jQuery.fn.focus_first = function() {
    var elem = $('input:visible', this).get(0);
    var select = $('select:visible', this).get(0);
    if (select && elem) {
	if (select.offsetTop < elem.offsetTop) {
	    elem = select;
	}
    }
    var textarea = $('textarea:visible', this).get(0);
    if (textarea && elem) {
	if (textarea.offsetTop < elem.offsetTop) {
	    elem = textarea;
	}
    }
  
    if (elem) {
	elem.focus();
    }
    return this;
}

jQuery(document).ready(function(e){
    jQuery("#object_cancel").bind("click",function(){refreshPage();})
    jQuery("#object_cancel").css({cursor:"pointer"});
    jQuery("#view_cancel").bind("click",function(){refreshPage();})
    jQuery("#view_cancel").css({cursor:"pointer"});
    jQuery('#login').ajaxForm(function() { 
	    refreshPage();
        });
    jQuery("#cmd_adduser").ajaxForm(function() {
	    refreshPage();
	});
    function refreshPage(){
	var sURL = unescape(window.location);
	window.location.href = sURL;
    }
    initPanels();
    function initPanels() {
	jQuery("#manual").hide();
	jQuery(".man_toggle").css({cursor:"pointer"});
	jQuery('.man_toggle').bind("click", function(){
		jQuery("#manual").slideToggle();
	    });
	jQuery("#view_mode").css({cursor:"pointer"});
	jQuery('#view_mode').bind("click", function(){
		jQuery("#view_cancel").removeClass("cancel_no");
		jQuery(this).addClass("view_mode_current");
		jQuery("a").removeAttr("href");
                for(var i = 0; i < ITEMS.length; i++) {
		    if(ITEMS[i].type == "location"){
		    jQuery("#"+ITEMS[i].id).bind("mouseover", function(){
			jQuery(this).addClass("select_tmpl_rel_hover");
			return false;
		    });
		    jQuery("#"+ITEMS[i].id).bind("mouseout", function(){
			jQuery(this).removeClass("select_tmpl_rel_hover");
			return false;
		    });
		    }
		}
		initItems("location");
	    });
	jQuery("#object_mode").css({cursor:"pointer"});
	jQuery('#object_mode').bind("click", function(){
		jQuery("#object_cancel").removeClass("cancel_no");
		jQuery(this).addClass("object_mode_current");
		jQuery("a").removeAttr("href");
                for(var i = 0; i < ITEMS.length; i++) {
		    if(ITEMS[i].type == "object"){
		    jQuery("#"+ITEMS[i].id).bind("mouseover", function(){
			jQuery(this).addClass("select_hover");
			return false;
		    });
		    jQuery("#"+ITEMS[i].id).bind("mouseout", function(){
			jQuery(this).removeClass("select_hover");
			return false;
		    });
		    }
		}
		initItems("object");
	    });
	jQuery("#users-groups").hide();
	jQuery(".users-groups_toggle").css({cursor:"pointer"});
	jQuery('.users-groups_toggle').bind("click", function(){
		jQuery("#users-groups").slideToggle();
	    });
	jQuery(".exit_cmd").css({cursor:"pointer"});
	jQuery('.exit_cmd').bind("click", function(){
		jQuery.get("/cmd/logout", function(){refreshPage();});
	    });
    }
    jQuery(document).keydown(
	function(e){
	    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	    if (key == "27") {
		jQuery("#login").show().focus_first();
	    }
	});
});
