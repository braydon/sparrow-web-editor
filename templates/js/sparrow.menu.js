/*
Sparrow, Menu Initialization Javascript
(c) Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


function setStat(data){
    var percent, remaining, total;
    stat = document.getElementById('stat');
    bar = jQuery('#stat_done');
    if (data.status == "uploading"){
    if (data.transfered){
        percent = Math.round(data.transfered/data.total*100)+"%";
        bar.animate({width:percent},"slow");
    }
    if (data.eta > 60){
        remaining = Math.round(data.eta/60) + " minutes";
    } else if (data.eta < 60){
        remaining = data.eta + " seconds";
    } else {
        remaining = null;
    }
    if (data.total > 1024.0){
        total = Math.round(data.total / 1024.0) + "MBs"
    } else if (data.total < 1024.0){
        total = Math.round(data.total) + "KBs"
    } else {
        total = null;
    }
    text = "<small><strong>" + percent + " of " + total + "</strong> @ " + Math.round(data.speed) + "KBs/s with " + remaining + " remaining. </small>";
    stat.innerHTML = text
    } else if (data.status == "done"){
        stat.innerHTML = "<small><strong>100% Completed</strong></small>";
        bar.animate({width:'100%'},"slow");
    }
}

function getStat(){
    jQuery.getJSON('/cmd/upload_stats',function(data){setStat(data)});
}

function startStat(){
    stat = setInterval(getStat, 9000);
}



jQuery(document).ready(function(e){
/*    jQuery(document).keydown(
	function(e){
	    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	    if (key == "17") {
                jQuery("#commands").css({display: "block",left:MOUSE[0], top:MOUSE[1] });
                COMMAND_MENU = true;
	    }
	}

     );
*/


    jQuery("#cancel").Tooltip({ delay: 0 });
    jQuery("#center").Tooltip({ delay: 0 });
    jQuery("#cancel").click(refreshPage);
    jQuery("#center").bind("mousedown",function(){
	drag = true
    });
    jQuery("#center").bind("mouseup",function(){
	drag = false;
    });
    walkLists("commands");
    jQuery("#form-imageset").submit(function() { 
	    /*alert("starting");*/
	    startStat();
            return false;
	});
});

function refreshPage(){
    var sURL = unescape(window.location);
    window.location.href = sURL;
}

function walkLists(ul_id) {
    children = jQuery("#"+ul_id).children("li");
    if(children){
	 jQuery.each(children, function(){
                bindEvents(this.id);
                children_ul = jQuery("#"+this.id).children("ul");
                if(children_ul){
		    jQuery.each(children_ul, function(){
                        walkLists(this.id);
                    });
                }
         });
    }
}

function bindEvents(id) {
    jQuery("#"+id+" > .command").bind("mouseover", function(){
        hideParentSiblings(id);
        showSiblings(id);
	showChildren(id);
        toggleList(id, true);
	return false;
    });

    jQuery("#"+id+" > .command").Tooltip({ delay: 0 });

    if (jQuery("#"+id+" > .command > form").is(":has('input[type=submit]')")) {
        jQuery("#"+id+" > .command > form").ajaxForm(function() { 
		refreshPage();
            return false;
        });
    } else if (jQuery("#"+id+" > .command").is(":has('form')")) {
        jQuery("#"+id+" > .command").click(function() {
            jQuery("#"+id+" > .command > form").ajaxSubmit(function() { 
		    refreshPage();
                    return false;
            });
	    return false;
        });
    }
}

function showChildren(id){
    children = jQuery("#"+id+"> ul").children("li");
    if(children){
        jQuery.each(children, function(){
            jQuery("#"+this.id+"> .command").removeClass("hide");
        });
    }
}

function hideParentSiblings(id){
    parent_id = jQuery("#"+id).parent().parent().attr("id");
    siblings = jQuery("#"+parent_id).siblings();
    if(siblings){
        jQuery.each(siblings, function(){
            toggleList(this.id, false);
            jQuery("#"+this.id+"> .command").addClass("hide");
        });
    }
}

function hideSiblingsChildren(id){
    siblings = jQuery("#"+id).siblings();
    if(siblings){
        jQuery.each(siblings, function(){
            jQuery("#"+this.id+">ul").addClass("hide");
        });
    }
}

function showSiblings(id){
    siblings = jQuery("#"+id).siblings();
    if(siblings){
        jQuery.each(siblings, function(){
            toggleList(this.id, false);
            jQuery("#"+this.id+"> .command").removeClass("hide");
            jQuery("#"+this.id+"> .command").removeClass("current");
        });
    }
}

function toggleList(id, is_off){
    if(is_off){
        jQuery("#"+id+"> .command").addClass("current");
        jQuery("#"+id+"> ul").removeClass("hide");
        hideSiblingsChildren(id);
        return false;
    } else {
        jQuery("#"+id+"> .command").removeClass("current");
        return false;

    }   
}
