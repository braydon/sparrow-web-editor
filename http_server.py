from sparrow.http import *

conf = open('/var/www/sample/config/.conf','w')
conf.write("".join([open('/var/www/sample/config/server.conf','r').read(), open('/var/www/sample/config/static.conf','r').read()]))
conf.close()

if __name__ == "__main__":
    cherrypy.quickstart(Root(), '/', config="/var/www/sample/config/.conf")
