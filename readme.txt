"""
Sparrow
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

Installation
============
============


Debian Lenny/Sid
================

Install dependencies
--------------------
$ apt-get install python2.5 (required)
$ apt-get install python-cherrypy3 (required)
$ apt-get install python-sqlalchemy (required)
$ apt-get install python-cheetah (required)
$ apt-get install python-mysqldb (optional for MySql support)

Setup a database
----------------
Create a database, and user for the site.
Dump start.dat into this newly created database.

Setup your configuration
------------------------
server.conf
  - update [database] with your the
    matching information for your database
  - update [root] info to match
    the full path to each of the resources
  - enter your sitename
  - enter your root password
  - update [objects] base_path to match
    the full path to your media directory
  - update [global] with your server info

static.conf
  - change all tools.staticdir.root to the 
    associated dir on your computer.

Run the site
------------
To use the internal HTTP Server:
$ cd path/to/your/site
$ python start_app.py

To use with mod_wsgi, and Apache (recommended):
(coming soon)

To use with Apache proxy, Supervisor2:
(coming soon)



Documentation
=============
=============

Modules Overview
================
- Users/Groups
Used for general login/logout, and user and group manipulation.

- Basic
Common controllers used for basic database, and filesystem tasks.

- Templates
Used for manipulating, changing for use by the recursive templates.

- URI
Used to record the relationship of 'data in the cloud' (objects), 
templates, sub-templates, and style, and behavior data. 

- Images
Used to upload, save, resize, crop and modify images in templates, and 
via the "ctrl" interface.

Modules: Users
--------------

- get_session()
- login()
- logout()
- get_user()

Modules: Basic
--------------

- get_object(oid)
Returns one object from the "objects_table" that matches the "oid" parameter. Every object returned will have relational data from other tables attatched to it.

- object_children(parent_oid)
Returns a list of objects from the "objects_table" that have a matching "parent_oid" parameter from the "heirarchy_table".

- insert_object(heirarchy, name, uri, permissions)
uri = "/ctrl/insert_object"
This will allow you to be able to insert an object 'after', 'before', or 'inside' another object. However, it is always possible to insert the same object into another object later, and is not actually 'inside' the other object. It inserts an object into the "object_table", as well as insert the name, and uri meta-data into "name_table". The parameters "name" and "uri" are not required, permissions will be default to "644" if not set.

- delete_object(oid)
uri = "/ctrl/delete_object"
This allows for a complete destruction of an object, and all it's meta-data. It's the inverse of "insert_object()" it will remove an object from the "objects_table" and cascade removal all relational meta-data in other tables.

- edit_name(oid, name, uri):
uri = "/ctrl/edit_name"
This allows for multiple names to be given to an object, as well as changing an exhisting name of an object. This changes the first name in the "name_table" for names matching the "oid" parameter.

- add_heirarchy(oid, parent_oid)
uri = "/ctrl/add_heirarchy"
This allows for a 'non-heirarchical', or 'multi-heirarchical' trees to be contructed. It allows for one object to be related to another. It inserts a row into the "heirarchy_table".

- permissions(object, user, group)
This will determine if a user has the privilages to be able to "read" and/or "write" the "object". Note: you can not pass an "oid", it requires passing it an object returned from "get_object()".

- change_order(parent_id, oid, index)
uri = "/ctrl/change_order"
This allows for the order of objects to be manually set rather than order them by other meta-data. Note: the order is not absolute, and is dependent on it's relationship between it's "parent_id", or the "oid" in that the list of objects share a relationship with. This allows for an object to be placed in different locations, and have a different order based on that particular pespective. This will insert into the "objects_order" the "parent_id", "oid", and "index".

- sort_by_order(objects)
This will take a list of objects returned from "object_children()", and sort them based on their order meta-data from the "order_table". This allows objects to be sorted in a custom way, rather than based on other kinds of 'arbitrary' meta-data. Note: you can note use an "oid" for this.

Modules: Templates
------------------

- get_template(vid, name)
Returns a template object, that will all of the attributes of the "Templates" class, not to be confused with the Cheetah "Template" class. Either the "vid", or the "name" can be used to return a template object.

- save_template(name, parent, vid, children)
uri = "/ctrl/save_template"
This can create a new template and save it to the filesystem and database, or change the "name", "parent", or "children" of an existing template, the "vid" is used as the unique identifier. If the "name" or "parent" are modified, it will rename the file in the 'tmpl' directory to match, and create a backup file with an *.bak extension. 
Unlike objects returned from "get_object()", templates are organized in strictly heirarchical tree structure, one template can not have two "parent"s. This is because the "parent" is used to form the filename for the template. The filename structure of a template is "parent_name.tmpl", where "parent", and "child" are parameters of "save_template". Furthermore, if the parent has a parent, then the filename would be "parent_parent_name.tmpl", and so on. This is so that all template names and tree structure are revealed in one directory.
The parameter "children" allow for the definition of internal 'uri' in that template, allowing successive templates to be plugged into that template. This can be infinitely recursive; allowing for a 'fibinachi', or 'organic' like combination of templates.

- delete_template(vid)
uri = "/ctrl/delete_template"
This is the inverse of "save_template()" and is completely destructive. It will delete the template from the filesystem and database.

Modules: Universal Resoure Idendifier (URI)
-------------------------------------------

- save_uri(uri, oid, children)
uri = "/ctrl/save_uri"
In this context a "uri" is used as the identifier for the combination of the following: 
1. a "vid" or template(*.tmpl) file
2. a object "oid", and a list of it's related, or child objects
3. "children"; other sub-"uri" to be included into the "uri" (optional)
4. style or behaviorial files such as *.css or *.js files. (in upcoming versions)

- get_uri(uri)
Returns a Location, or URI object as created by "save_uri()", matched using the "uri" from the "relations_table".

- delete_uri(uri)
uri = "/ctrl/delete_uri"
Inverse of "save_uri()" and will destroy the matched "uri" data in the "relations_table".

Modules: Images
---------------

- save_image(oid, index, file)
uri = "/ctrl/save_image"
Records an image to the database, and uploads/saves the image to the filesystem into the configured 'images' directory. The filename for the image is "oid_index.ext", where "oid" is the object id number, "index" is identifier for multiple 'versions' of the same image generated by other scripts, and "ext" is the file format that the image. The parameter "file" is a file from an html form with 'input type=file'.

- get_image(oid, index, src, modified)
This can do one of a few things; 
1. Return an already created image in the database based on the "oid", and "index".
2. Record and Return a new generated image into the database, with a 'new' "index", "modified", "src".
3. Record and Return and already created image with a 'new' "modified".
An image object is returned from the "images_table" that matches the "oid" and "index", with "src", "modified", "height", and "width" attributes.

- resize_image(oid, settings)
This takes the image associated with "oid" and 'converts' it using ImageMagick with the "settings", and records the new image using "get_image()". The file is saved in the format "oid_index.ext". 
Settings are defined as a dictionary as follows:
{"index":'0', "width":'120x', "height":'x120<', "gravity":'center', "crop":'80x80+0+0} For more information about settings, reference ImageMagick command line documentation, there is a whole lot more possible that this.

