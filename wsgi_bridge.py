import sys, os
sys.path.append('/var/www/sample')
sys.stdout = sys.stderr

import atexit
import threading

from sparrow.http import *

conf = open('/var/www/sample/config/.conf','w')
conf.write("".join([open('/var/www/sample/config/server.conf','r').read(), open('/var/www/sample/config/static.conf','r').read()]))
conf.close()


#cherrypy.config.update({'tools.sessions.on':True})
#cherrypy.config.update({'tools.dbconn.on':True})

if cherrypy.engine.state == 0:
    cherrypy.engine.start(blocking=False)
    atexit.register(cherrypy.engine.stop)

application = cherrypy.Application(Root(), '/')
application.merge(config="/var/www/sample/config/.conf")


