import os
import sys
import subprocess

from optparse import OptionParser
from sparrow.settings import Settings

settings = Settings()
user = settings.database.user
password = settings.database.password
database_name = settings.database.database_name
dump_file = "".join([os.getcwd(),"/db/backup.dat"])

def dump():
    print "Preparing the dump database '%s' to '%s'" % (database_name, dump_file)
    dump_cmd = "mysqldump -u %s -p %s > %s" % (user, database_name, dump_file)
    os.popen(dump_cmd)
    print "Dumped database %s to %s" % (database_name, dump_file)

def load():
    print "Preparing to import dump file '%s' to database '%s'" % (dump_file, database_name)
    load_cmd = "mysql -u %s -p %s < %s" % (user, database_name, dump_file)
    os.popen(load_cmd)
    print "Imported dump file '%s' to database '%s'" % (dump_file, database_name)

def walk_clean(d):
    print "walking %s" % d
    for name in os.listdir(d):
        path = os.path.join(d, name)
        if os.path.isdir(path):
            walk_clean(path)
        elif name.endswith('~'):
            os.remove(path)
	    print "removing %s" % (path)
	elif name.endswith('.bak'):
	    os.remove(path)
	    print "removing %s" % (path)
	elif name.endswith('.pyc'):
	    os.remove(path)
	    print "removing %s" % (path)

def commit(message):
    print "starting Git add"
    add_cmd = "git add ."
    p = subprocess.Popen(add_cmd, shell=True, stdout=subprocess.PIPE)
    (stdout, stderr) = p.communicate()
    if stdout:
        print stdout
    elif stderr:
	print stderr

    print "starting Git commit"
    commit_cmd = 'git commit -m "%s"' % (message)
    p = subprocess.Popen(commit_cmd, shell=True, stdout=subprocess.PIPE)
    (stdout, stderr) = p.communicate()
    if stdout:
        print stdout
    elif stderr:
	print stderr


parser = OptionParser()
parser.add_option("-m","--message", dest="message", help="message to be used for task: commit")
(options, args) = parser.parse_args()

for arg in args:
    if arg == "dump":
        dump()
    elif arg == "load":
        load()
    elif arg == "commit":
        dump()
	print "Removing all files that end with '~', '.bak', and '.pyc'"
        walk_clean(os.getcwd())
	if options.message == None:
	    print "*no message? aborting commit."
        else:
            commit(options.message)
       
