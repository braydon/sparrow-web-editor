"""
Sparrow, Object Models Package
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import datetime

from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, DateTime

metadata = MetaData()

class Audio(object):
    def __init__(self, oid, src):
        self.oid = oid
        self.src = src
    def __repr__(self):
        return "<Audio('%s','%s')>" % (self.oid, self.src)

audio_table = Table('objects_audio', metadata,
    Column('id', Integer, primary_key=True),
    Column('oid',Integer, ForeignKey("objects.oid")),
    Column('src', String(1000))
)

class Group(object):
    def __init__(self, name):
        self.name = name
    def __repr__(self):
        return "<Group('%s','%s','%s')>" % (self.name)

class Html(object):
    def __init__(self, oid, src, id=None):
        self.id = id
        self.oid = oid
        self.src = src
    def __repr__(self):
        return "<Html('%s','%s')>" % (self.oid, self.src)

html_table = Table('objects_html', metadata,
    Column('id', Integer, primary_key=True),
    Column('oid',Integer, ForeignKey("objects.oid")),
    Column('src', String(1000))
)

class Img(object):
    def __init__(self, oid, src, dict=None, width=None, height=None):
        self.oid = oid
        self.src = src
        self.dict = dict
        self.width = width
        self.height = height
    def __repr__(self):
        return "<Image('%s','%s','%s')>" % (self.oid, self.src, self.dict)

image_table = Table('objects_image', metadata,
    Column('oid',Integer, ForeignKey("objects.oid")),
    Column('src', String(1000), primary_key=True),
    Column('dict', String(256)),
    Column('width', String(100)),
    Column('height', String(100))
)

class Name(object):
    def __init__(self, oid, name, uri=None, id=None):
        self.id = id
        self.oid = oid
        self.name = name
        self.uri = uri
    def __repr__(self):
        return "<Name('%s','%s','%s')>" % (self.oid, self.name, self.uri)
    def __str__(self):
        return self.name

name_table = Table('objects_name', metadata,
    Column('id',Integer, primary_key=True),
    Column('oid',Integer, ForeignKey("objects.oid")),
    Column('name', String(100), nullable=False),
    Column('uri', String(100))
)

class Objects(object):
    def __init__(self, permissions, oid=None, modified=None, created=None, view=None, order=None, name=None, image=None, html=None):
	self.permissions = permissions
        self.oid = oid
        self.modified = modified
        self.created = created
        self.order = order
        self.name = name
        self.image = image
        self.html = html
        self.view = view
        
    def __repr__(self):
        return "<Objects('%s')>" % (self.oid)
    def __str__(self):
        return self.oid

objects_table = Table('objects', metadata,
    Column('oid',Integer, primary_key=True),
    Column('modified',DateTime, onupdate=datetime.datetime.utcnow()),
    Column('created',DateTime, default=datetime.datetime.utcnow()),
    Column('permissions',String(100), default='644'),
    Column('view',String(100))
)

class Order(object):
    def __init__(self, oid, parent_oid, order, id=None):
        self.id = id
        self.oid = oid
        self.parent_oid = parent_oid
        self.order = order
    def __repr__(self):
        return "<Order('%s','%s','%s')>" % (self.oid, self.parent_oid, self.order)

order_table = Table('objects_order', metadata,
    Column('id',Integer, primary_key=True),
    Column('oid',Integer, ForeignKey("objects.oid"), nullable=False),
    Column('parent_oid',Integer, nullable=False),
    Column('order',Integer, nullable=False)
)

class Owner(object):
    def __init__(self, oid, user, group):
        self.oid = oid
        self.user = user
        self.group = group
    def __repr__(self):
        return "<Owner('%s','%s','%s')>" % (self.oid, self.owner, self.group)

owner_table = Table('objects_owner', metadata,
    Column('id',Integer, primary_key=True),
    Column('oid',Integer, ForeignKey("objects.oid"), nullable=False),
)

class Heirarchy(object):
    def __init__(self, oid, parent_oid):
        self.oid = oid
        self.parent_oid = parent_oid
    def __repr__(self):
        return "<Heirarchy('%s','%s')>" % (self.oid, self.parent_oid)

heirarchy_table = Table('objects_heirarchy', metadata,
    Column('id',Integer, primary_key=True),
    Column('oid',Integer, ForeignKey("objects.oid"), nullable=False),
    Column('parent_oid',Integer, nullable=False)
)

class Templates(object):
    def __init__(self, name, parent, vid=None, children=None, source=None):
	self.name = name
        self.parent = parent
        self.vid = vid
        self.children = children
        self.source = source
    def __repr__(self):
        return "<Templates('%s','%s','%s')>" % (self.name, self.parent, self.vid)

templates_table = Table('templates', metadata,
    Column('vid',Integer, primary_key=True),
    Column('name',String(100)),
    Column('source', String(255)),
    Column('parent',Integer),
    Column('children', String(1000))
)

class TemplateRelation(object):
    def __init__(self, location, oid=None, vid=None, children=None):
        self.location = location
        self.oid = oid
        self.vid = vid
        self.children = children
    def __repr__(self):
        return "<TemplateRelation('%s','%s',%s','%s')>" % (self.location, self.oid, self.vid, self.children)

template_rels_table = Table('template_relations', metadata,
    Column('location',String(1000), primary_key=True),
    Column('oid',Integer),
    Column('vid', Integer, ForeignKey("templates.vid")),
    Column('children', String(1000))
)

class User(object):
    def __init__(self, username, password, full_name, email):
        self.username
        self.password
        self.full_name
        self.email
        self.group
    def __repr__(self):
        return "<User('%s','%s','%s','%s','%s')>" % (self.username, self.password, self.full_name, self.email, self.group)

class Video(object):
    def __init__(self, oid, src):
        self.oid = oid
        self.src = src
    def __repr__(self):
        return "<Video('%s','%s')>" % (self.oid, self.src)

video_table = Table('objects_video', metadata,
    Column('id', Integer, primary_key=True),
    Column('oid',Integer, ForeignKey("objects.oid")),
    Column('src', String(1000))
)
