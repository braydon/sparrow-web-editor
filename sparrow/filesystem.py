"""
Sparrow, File System Management Package
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import os
import shutil
import cgi
import tempfile
import codecs

import cherrypy
from sparrow.settings import Settings

settings = Settings()
base_path = settings.object.base_path


class FileSystem(object):
    def __init__(self):
        self.images = FS(settings.object.images)
        self.video = FS(settings.object.video)        
        self.audio = FS(settings.object.audio)
        self.html = FS(settings.object.html) 
        self.templates = FS(no_static=settings.root.views_path)
        self.tmp = FS(no_static=settings.root.tmp_path)

class FS(object):
    def __init__(self,uri=None,no_static=None):
        if no_static:
            self.path = no_static
            self.uri = None
        else:
            self.path = base_path+uri
            self.uri = uri

    def upload(self, src, dst):
        size = 0
        f = open(self.path+"/"+dst, 'wb')
        while True:
            data = src.file.read(1024 * 8)
            if not data:
                break
            size += len(data)
            f.write(data)
        f.close()

    def rename(self,src,dst):
        shutil.copyfile(self.path+"/"+src, self.path+"/"+src+".bak")
        os.rename(self.path+"/"+src,self.path+"/"+dst)

    def create(self,src):
        try:
            f = open(self.path+"/"+src,'r')
        except:
            f = open(self.path+"/"+src,'w')
        f.close()
        print "Made file %s" % (self.path+"/"+src)


    def create_write(self,src,new):
        try:
            f = open(self.path+"/"+src,'r')
        except:
            f = open(self.path+"/"+src,'w')
        finally:
            f.write(new)
            f.close()

    def write(self,src,new):
        f = codecs.open(self.path+"/"+src,'w')        
	f.write(new)
        f.close()

    def read(self,src):
        try:
            f = open(self.path+"/"+src,'r')
            output = f.read()
            f.close()
            return output
        except:
            print "Error: error reading html file %s" % (self.path+"/"+src)

    def delete(self,src,backup=True):
        if backup:
            try:
                os.rename(self.path+"/"+src,self.path+"/"+src+".bak")
            except:
                pass
        else:
            os.remove(self.path+"/"+src)
            

