"""
Sparrow, Hyper Text Transfer Protocol Package
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import time
import tempfile
import simplejson as json

import cherrypy
from cherrypy import tools
from sparrow.settings import Settings
from sparrow.database import Database
from sparrow.objects import render

db = Database()
settings = Settings()
commands = __import__('exposed', globals(), locals(),['sparrow'])

cherrypy.file_transfers = {}

class ProgressFile(object):
    """ Used to hold information about an upload """
    def __init__(self, buf, *args, **kwargs):
        self.file_object = tempfile.TemporaryFile(*args, **kwargs)
        self.transfered = 0
        self.buf = buf
        self.pre_sized = float(cherrypy.request.headers['Content-length'])
        self.speed = 1
        self.remaining = 0
        self.eta = 0
        self._start = time.time()

    def write(self, data):
        now = time.time()
        self.transfered += len(data)
        upload_timeout = getattr(cherrypy.thread_data, 'upload_timeout', False)
        if upload_timeout:
            if (now - self._start) > upload_timeout:
                raise Upload_TimeoutError

        upload_maxsize = getattr(cherrypy.thread_data, 'upload_maxsize', False)
        if upload_maxsize:
            if self.transfered > upload_maxsize:
                raise Upload_MaxSizeError

        self.speed = self.transfered / (now - self._start)

        upload_minspeed = getattr(cherrypy.thread_data, 'upload_minspeed', False)
        if upload_minspeed:
            if self.transfered > (5 * self.buf): # gives us a reasonable wait period.
                if self.speed < upload_minspeed:
                    raise Upload_UpSpeedError

        self.remaining = self.pre_sized - self.transfered

        if self.speed == 0: self.eta = 9999999
        else: self.eta = self.remaining / self.speed

        return self.file_object.write(data)

    def seek(self, pos):
        self.post_sized = self.transfered
        self.transfered = True
        return self.file_object.seek(pos)

    def read(self, size):
        return self.file_object.read(size)


class FieldStorage(cherrypy._cpcgifs.FieldStorage):
    """
    Used to store information about an upload"""

    def __del__(self, *args, **kwargs):
        try:
            dcopy = cherrypy.file_transfers[cherrypy.request.remote.ip].copy()
            for key, val in dcopy.iteritems():
                if val.transfered == True:
                    del cherrypy.file_transfers[cherrypy.request.remote.ip][key]
            del dcopy
            if len(cherrypy.file_transfers[cherrypy.request.remote.ip]) == 0:
                del cherrypy.file_transfers[cherrypy.request.remote.ip]

        except KeyError:
            pass

    def make_file(self, binary=None):
        print "mf %s" % cherrypy.request.remote.ip
        fo = ProgressFile(self.bufsize)
        if cherrypy.file_transfers.has_key(cherrypy.request.remote.ip):
            cherrypy.file_transfers[cherrypy.request.remote.ip]\
                    [self.filename] = fo
        else:
            cherrypy.file_transfers[cherrypy.request.remote.ip]\
                    = {self.filename:fo}

        return fo

cherrypy._cpcgifs.FieldStorage = FieldStorage

class Root():

     def default(self, *args, **kwargs):
          self.cmd = commands
          location_st = ""
          for arg in args:
               location_st += "/"+arg
          ret = render(args, kwargs, location_st)
          return ret
     default.exposed = True

class DBConnTool():
     def __init__(self):
          self.db = Database()
          self.ended = {}          
          self._name = "dbconn"

     def close_conn(self):
          if hasattr(cherrypy.request, "dbconn"):
               try:
                    cherrypy.request.dbconn.close()
               except:
                    cherrypy.log(format_exc(), "SQLALCHEMY")
                    
     def _setup(self):
          cherrypy.request.dbconn = self.db.db.engine.connect()          
          cherrypy.request.hooks.attach('on_end_request', self.close_conn)

tools.dbconn = DBConnTool()
          
