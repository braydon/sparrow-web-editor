"""
These are the loaded packages for URI exposure.
For example: "/ctrl/save_image" will be available from
the ctrl.image package, by decorating save_image with
@cherrypy.exposed. Root() in ctrl.http package imports these
into the URI "ctrl".
"""

from sparrow.objects import *

