"""
Sparrow, Menu Package
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from Cheetah.Template import Template

from sparrow.settings import Settings
from sparrow.objects import get_object
from sparrow.uri import get_uri
from sparrow.templates import walk_objects, walk_templates
from sparrow.database import Database
from sparrow.models import TemplateRelation

settings = Settings()
db = Database()

def menu(object_id=None, location_id=None):
    if object_id:
        try:
            selected = get_object(object_id)
        except:
            selected = get_object(object_id)
        tmpl = settings.root.radial_menu_path+"/objects.tmpl"
        results = Template(file=tmpl, searchList=[locals()])
        return results.respond()
    elif location_id:
        try:
            location = get_uri(location_id)
        except:
            try:
                location = get_uri(location_id)
            except:
                location = TemplateRelation(location=location_id)
        root_object = get_object(oid="19")
        template_list = walk_templates(parent_vid=None, location_id=location_id)
        objects = walk_objects(root_object, location_id=location_id)
        tmpl = settings.root.radial_menu_path+"/views.tmpl"
        results = Template(file=tmpl, searchList=[locals()])
        return results.respond()
menu.exposed = True
