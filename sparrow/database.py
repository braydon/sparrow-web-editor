"""
Sparrow, Database Management Package
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from sparrow.models import *
from sparrow.settings import Settings

settings = Settings()

class DB(object):
    def __init__(self, engine=None, connection=None, session=None):
        self.engine = engine
        self.connection = connection

def get_database(info):
    # place this info into the settings, or config instead of doing crazy string building.
    info = info
    info_list = []
    info_list.append(info.database)
    info_list.append("://")
    info_list.append(info.user)
    info_list.append(":")
    info_list.append(info.password)
    info_list.append("@")
    info_list.append(info.host)
    info_list.append(":")
    info_list.append(info.port)
    info_list.append("/")
    info_list.append(info.database_name)
    info_url_list = []
    for info in info_list:
        info_url_list.append(info.replace('\"',''))
    info_url = "".join(info_url_list)
    db = DB()
    #db.engine = create_engine(info_url, pool_recycle=3600, pool_timeout=10, max_overflow=10, pool_size=20, echo_pool=True, encoding='utf-8', strategy='threadlocal')    
    #db.engine = create_engine(info_url, pool_recycle=25200, pool_timeout=60, max_overflow=15, pool_size=20, encoding='utf-8')
    db.engine = create_engine(info_url, pool_recycle=3600, pool_timeout=10, max_overflow=10, pool_size=20, encoding='utf-8')
    return db

class Database:
    db = get_database(settings.database)
    #Session = scoped_session(sessionmaker(bind=db.engine, autoflush=False, transactional=True))
    #Session = scoped_session(sessionmaker(bind=db.engine))
    #Session = sessionmaker(bind=db.engine, autoflush=False, transactional=True)
    __shared_state = {}
    def __init__(self):
        self.__dict__ = self.__shared_state

        

