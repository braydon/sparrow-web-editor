"""
Sparrow, Object Management Package
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import os
import struct
from zipfile import *
from tarfile import *
import datetime
import cgi
import tempfile
import uuid
import sys
import traceback
from math import ceil
import operator
from copy import copy
import pytz
import time
from functools import partial

import cherrypy
from cherrypy._cperror import HTTPError
from sqlalchemy.sql import select, bindparam
from sqlalchemy.exceptions import InvalidRequestError
from sparrow.database import Database
from sparrow.filesystem import FileSystem
from sparrow.settings import Settings
from sparrow.models import Objects, objects_table, Heirarchy, heirarchy_table, Name, \
     name_table, Order, order_table, Img, image_table, Html, html_table, \
     TemplateRelation, template_rels_table, Templates, templates_table
from pygments import highlight
from pygments.lexers import PythonLexer, guess_lexer, guess_lexer_for_filename
from pygments.formatters import HtmlFormatter
from pygments.styles import get_style_by_name
import textile
from Cheetah.Template import Template
from Cheetah.NameMapper import NotFound
from cherrypy._cperror import HTTPError
import simplejson

utc_tz = pytz.utc
local_tz = pytz.timezone("America/Los_Angeles")
settings = Settings()

start_tag = "[python]"
end_tag = "[/python]"

fs = FileSystem()
settings = Settings()

def get_object(oid):
    dbconn = cherrypy.request.dbconn
    result = dbconn.execute(select([objects_table], objects_table.c.oid==oid))
    row = result.fetchone()
    result.close()
    if row:
        selected = Objects(row['permissions'],row['oid'],row['modified'], \
                           row['created'],row['view'])
    else:
        return None
    permissions = get_permissions(selected, get_session())
    if permissions['read']:
        order = dbconn.execute(select([order_table], order_table.c.oid==oid)).fetchone()
        if order:
            selected.order = [Order(order['oid'],order['parent_oid'],order['order'],order['id'])]
        name = dbconn.execute(select([name_table], name_table.c.oid==oid)).fetchone()
        if name:
            selected.name = [Name(name['oid'],name['name'],name['uri'],name['id'])]
        images = dbconn.execute(select([image_table], image_table.c.oid==oid)).fetchall()
        if images:
            selected.image = []
            for image in images:
                selected.image.append(Img(image['oid'],image['src'],image['dict'],image['width'],image['height']))
        html = dbconn.execute(select([html_table], html_table.c.oid==oid)).fetchone()
        if html:
            selected.html = [Html(html['oid'],html['src'],html['id'])]
        return selected
    else:
        return None

def object_children(oid):
    dbconn = cherrypy.request.dbconn
    parent = get_object(oid)
    selected = []
    if parent:
        rows = dbconn.execute(select([heirarchy_table.c.oid], heirarchy_table.c.parent_oid==parent.oid))
        for row in rows:
            obj = get_object(row[0])
            if obj:
                selected.append(obj)
        rows.close()
        if len(selected) > 0:
            return selected
        else:
            return None
    else:
        return None
                
@cherrypy.expose
def insert_object(parent_id, name, uri=None, uri_base=None, perms=644, callback=False, text=None, image_src=None, image_index=None):
    dbconn = cherrypy.request.dbconn
    parent = get_object(parent_id)
    permissions = get_permissions(parent, get_session())
    if permissions['write']:
        result = dbconn.execute(objects_table.insert(values={'perms':perms, 'created':datetime.datetime.utcnow()}))
        oid = result.last_inserted_ids()[0]
        result.close()
        result = dbconn.execute(heirarchy_table.insert(values={'oid':oid,'parent_oid': parent_id}))
        result.close()
        result = dbconn.execute(name_table.insert(values={'oid':oid, 'name': name, 'uri': uri}))
        if text:
            save_html(oid, text)
        #TODO make sure the below returns proper error code
        try:
            save_image(oid, image_index, image_src)
        except:
            pass
        if uri and uri_base:
            copy_uri(uri_base, uri)
        if callback:
            return get_object(oid)

@cherrypy.expose
def delete_object(object_id):
    dbconn = cherrypy.request.dbconn
    selected = get_object(object_id)
    permissions = get_permissions(selected, get_session())
    if permissions['write']:
        dbconn.execute(objects_table.delete(objects_table.c.oid == object_id))
        dbconn.execute(name_table.delete(name_table.c.oid == object_id))
        dbconn.execute(order_table.delete(order_table.c.oid == object_id))
        dbconn.execute(heirarchy_table.delete(heirarchy_table.c.oid == object_id))
        dbconn.execute(image_table.delete(image_table.c.oid == object_id))
        if hasattr(selected, "image") and isinstance(selected.image, list):
            fs.images.delete(selected.image[0].src)        
        dbconn.execute(html_table.delete(html_table.c.oid == object_id))
        if hasattr(selected, "html") and isinstance(selected.html, list):
            if selected.html != None:
                fs.html.delete(selected.html[0].src)                

def get_oid(a):
    return int(a.oid)

def get_section(oid,objs,size,offset=0):
    try: end = int(ceil(float(map(get_oid,objs).index(int(oid))+1+offset)/float(size))*size)
    except ValueError: raise HTTPError(404, body="404 Not Found")
    return objs[int(end-size):end]


def read_html(source):
    html = fs.html.read(source)
    return html

def parse_code(text):
    if text.find(start_tag) == -1:
        return textile.textile(text, encoding='utf-8', output='utf-8')
    else:
        start = text.find(start_tag)
        start_code = start+len(start_tag)
        end_code = text.find(end_tag)
        end = end_code+len(end_tag)
        code_snip = text[start_code:end_code]
        result = highlight(code_snip, PythonLexer(), HtmlFormatter(style='emacs'))
        text = textile.textile(text[:start], encoding='utf-8', output='utf-8') + result + parse_code(text[end:])
        return text

@cherrypy.expose        
def save_html(oid, text=None, html=None, file=None):
    dbconn = cherrypy.request.dbconn
    selected = get_object(oid)
    permissions = get_permissions(selected, get_session())
    if permissions["write"]:
        dst = str(oid)+".html"
        if file:
            fs.html.upload(file,dst)
        elif text:
            fs.html.create(dst+".text")
            fs.html.write(dst+".text", text)
            html = textile.textile(text)
            fs.html.create(dst)
            fs.html.write(dst, html)
        else:
            fs.html.create(dst)
            fs.html.write(dst, html)

        if selected.html:
            result = dbconn.execute(html_table.update(html_table.c.oid==oid), src = dst)
            result.close()
        else:
            result = dbconn.execute(html_table.insert(values={'oid':oid,'src':dst}))
            result.close()

@cherrypy.expose
def save_html_and_code(oid, text=None, html=None, file=None):
    dbconn = cherrypy.request.dbconn    
    selected = get_object(oid)
    permissions = get_permissions(selected, get_session())
    if permissions["write"]:
        dst = oid+".html"
        if file:
            fs.html.upload(file,dst)
        elif text:
            fs.html.create(dst+".text")
            fs.html.write(dst+".text", text)
            html = parse_code(text)
            fs.html.create(dst)
            fs.html.write(dst, html)
        else:
            fs.html.create(dst)
            fs.html.write(dst, html)
        if selected.html:
            result = dbconn.execute(html_table.update(html_table.c.oid==oid), src = dst)
            result.close()            
        else:
            result = dbconn.execute(html_table.insert(values={'oid':oid,'src':dst}))
            result.close()

@cherrypy.expose
def save_code(oid, code=None, file=None):
    dbconn = cherrypy.request.dbconn
    code_file = oid+".code"
    html_file = oid+".html"
    if file:
        fs.html.upload(code_file)
        lexer = guess_lexer_for_filename(fs.html.path+"/"+code_file)
    if code:
        fs.html.create_write(code_file, code)
        lexer = guess_lexer(code)
    formatter = HtmlFormatter
    html = highlight(code, lexer, formatter)
    fs.html.create_write(html_file, html)
    if selected.html:
        result = dbconn.execute(html_table.update(html_table.c.oid==oid), src = dst)
        result.close()            
    else:
        result = dbconn.execute(html_table.insert(values={'oid':oid,'src':dst}))
        result.close()                

@cherrypy.expose
def save_image(object_id, index, src, image=None):
    dbconn = cherrypy.request.dbconn
    selected = get_object(object_id)
    permissions = get_permissions(selected, get_session())
    if permissions['write']:
        if src.type == 'image/jpeg':
            image_ext = '.jpg'
        elif src.type == 'image/gif':
            image_ext = '.gif'
        elif src.type == 'image/png':
            image_ext = '.png'
        else:
            image_ext = False
        if image_ext:
            dst = str(object_id)+"_original"+image_ext
            fs.images.upload(src,dst)
            need_to_append = True
            source = str(fs.images.uri+"/"+dst)
            if isinstance(selected.image, list):
                for image in selected.image:
                    if image.src == source:
                        need_to_append = False
            if need_to_append:
                result = dbconn.execute(image_table.insert(values={'oid': object_id, 'src': source, 'dict': 'original'}))
                result.close()

@cherrypy.expose
def upload_stats(*args, **kwargs):
    print "us %s" % cherrypy.request.remote.ip
    try:
        stat = cherrypy.file_transfers[cherrypy.request.remote.ip]
        # Convert everything to KBs and return
        for key, val in stat.iteritems():
            speed = '%9.2f' % (val.speed / 1024.0)
            total = '%9.2f' % (val.pre_sized / 1024.0)
            transfered = '%9.2f' % (val.transfered / 1024.0)
            eta = str(int(val.eta))
                
            done = val.transfered / val.pre_sized
            done = str(300 * done)
            """Logger.info("Upload Stats: fn: %s speed: %s total: %s transferred: %s" % \
            (key, speed, total, transfered))"""
            yield simplejson.dumps({'filename':key, 'speed': speed, 'total':total, \
                              'transfered':transfered, 'eta': eta, 'done':done, \
                              'status': 'uploading'})
    except KeyError:
        """Logger.info("Upload Stats: returning done")"""
        # If there are no entries by our IP, then we have nothing.
        yield simplejson.dumps({'status':'done'})
        

@cherrypy.expose
def save_image_set(parent_id, perms, the_file):
    dbconn = cherrypy.request.dbconn
    ignore_hidden_files = True
    extentions = [".jpg",".jpeg",".tif",".tiff",".png",".gif"]
    ignore_prefixes = ["__",".","._"]
    selected = get_object(parent_id)
    permissions = get_permissions(selected, get_session())
    if permissions["write"]:
        tmp_filename = str(selected.oid)+"_image_set"
        fs.tmp.upload(the_file,tmp_filename)
        if is_zipfile(fs.tmp.path+"/"+tmp_filename):
            archive = ZipFile(fs.tmp.path+"/"+tmp_filename,'r')
            archive_members = archive.namelist()
            archive_images = []
            for member_name in archive_members:
                if any(member_name.startswith(value) for value in ignore_prefixes) and ignore_hidden_files:
                    pass
                else:
                    ext = os.path.splitext(member_name)[-1]
                    if any(ext.lower() == value for value in extentions):
                        archive_images.append(member_name)

            for image_name in archive_images:
                selected = insert_object(parent_id, name="", uri="", perms=perms, callback=True)
                ext = os.path.splitext(image_name)[-1]
                dst = str(selected.oid)+"_original"+ext
                image_file = archive.read(image_name)
                fs.images.create_write(dst, image_file)
                result = dbconn.execute(image_table.insert(values={'oid':selected.oid,'src':fs.images.uri+"/"+dst,'dict':"original"}))
                result.close()
        
        elif is_tarfile(fs.tmp.path+"/"+tmp_filename):
            #TODO: MAKE THIS WORK FOR TAR FILES
            archive = False
            try:
                archive = open(fs.tmp.path+"/"+tmp_filename, 'r:gz')
            except:
                archive = open(fs.tmp.path+"/"+tmp_filename, 'r:bz2')
            else:
                print "Error: unkown archive type."
            if archive:
                archive_members = archive.getmembers()
                archive_images = []
                for member in archive_members:
                    extentions = [".jpg",".jpeg",".tif",".tiff",".png",".gif"]
                    ext = os.path.splitext(member.name)[-1]
                    if any(ext.lower() == value for value in extentions):
                        archive_images.append(member)
                for image_member in archive_images:
                    selected = insert_object(parent_id, name="", uri="", perms=perms, callback=True)
                    ext = os.path.splitext(image_member.name)[-1]
                    filename = str(selected.oid)+"_original"+ext
                    image_file = archive.extractfile(image_member)
                    fs.images.create_write(filename, image_file.read())
                    selected.image.append(Img(selected.oid, fs.images.uri+dst, "original"))
                    db.session.save_or_update(selected)
                    db.session.commit()
                    db.session.expire(selected)

        else:
            print "Error: unkown archive type."
        try:
            fs.tmp.delete(fs.tmp.path+"/"+tmp_filename, backup=False)
        except:
            "Print, no file to clean up after."

#save_image_set._cp_config = {'request.FieldStorage' : FieldStorage}
    
def get_image(object_id, dict=False, src=False):
    dbconn = cherrypy.request.dbconn
    selected = get_object(object_id)
    permissions = get_permissions(selected, get_session())
    if permissions['read']:
        image = False
        count = 0
        for img in selected.image:
            if img.dict == dict:
                image = selected.image[count]
            count = count+1
        if image:
            return image

        if dict and src:
            result = dbconn.execute(image_table.insert(values={'oid': object_id, 'src': src, 'dict': dict}))
            result.close()
            return get_image(object_id, dict)
        else:
            return False
    else: 
        return False

class NoOriginalImage(Exception):
    pass

def resize_image(object_id, image_settings):
    original = get_image(object_id, "original")
    if original:
        pass
    else:
        raise NoOriginalImage
    ext = os.path.splitext(original.src)[1]
    dst = str(object_id)+"_"+image_settings["dict"]+str(ext)
    resize = get_image(object_id, image_settings["dict"], fs.images.uri+"/"+dst)
    path = settings.object.base_path
    try:
        original_time = os.path.getmtime(path + original.src)
    except:
        original_time = 0
    try:
        resize_time = os.path.getmtime(path + resize.src)
    except:
        resize_time = 0
    if original_time > resize_time:
        needs_update = True
    else:
        needs_update = False

    if needs_update:
        src = fs.images.path+"/"+str(original.oid)+"_original"+ext
        magick_cmd = "convert %s " % (src)
        try:
            magick_cmd += '-resize %s ' % (image_settings["width"])
        except:
            pass
        try:
            magick_cmd += '-resize %s ' % (image_settings["height"])
        except:
            pass
        try:
            magick_cmd += '-gravity %s ' % (image_settings["gravity"])
        except:
            pass
        try:
            magick_cmd += '-crop %s +repage ' % (image_settings["crop"])
        except:
            pass
        magick_cmd += fs.images.path+"/"+dst
        os.popen(magick_cmd)
        get_image(object_id, image_settings["dict"], src=fs.images.uri+"/"+dst)


def _text_to_filename(the_text, text_settings):
    unique_str = the_text \
        + str(text_settings["font"]) \
        + str(text_settings["pointsize"]) \
        + str(text_settings["background"]) \
        + str(text_settings["fill"])
    return str(uuid.uuid5(uuid.NAMESPACE_OID, unique_str)) + str(text_settings["extension"])

def text_to_image(the_text, text_settings):
    """
    Creates an image using the defined typeface and text string.
    Returns an html image tag with the link to the image.
    Example settings:
    settings = {"font":"GothiThi.pfa","pointsize":"72", "background":"lightblue", "fill":"green", "extension":".png"}

    """
    base_path = str(settings.object.base_path)
    dst_dir = base_path + str(settings.object.text_to_image)
    font_path = base_path + str(settings.object.fonts) + "/" + str(text_settings["font"])
    
    filename = _text_to_filename(the_text, text_settings)
    dst = dst_dir + "/" + filename
    dst_url = "/text_to_image/" + filename
    
    if not os.path.exists(dst_dir):
        os.mkdir(dst_dir)

    if not os.path.exists(dst):
        magick_cmd = "convert "
        magick_cmd += '-font %s ' % font_path
        magick_cmd += '-pointsize %s ' % (text_settings["pointsize"])
        magick_cmd += '-background %s ' % (text_settings["background"])
        magick_cmd += '-fill %s ' % (text_settings["fill"])
        magick_cmd += 'label:"' + the_text + '" -trim +repage ' + dst
        os.popen(magick_cmd)

    return "<img src='%s' alt='%s' />" % (dst_url, the_text)

@cherrypy.expose
def edit_name(object_id, name, uri):
    dbconn = cherrypy.request.dbconn
    selected = get_object(object_id)
    permissions = get_permissions(selected, get_session())
    if permissions['write']:
        result = dbconn.execute(name_table.update(name_table.c.oid == object_id), name=name, uri=uri)
        result.close()

def archive_dict(a,b,timezone=None):
    utc_dt = b.created.replace(tzinfo=utc_tz)
    local_dt = utc_dt.astimezone(local_tz)
    y = local_dt.year
    m = local_dt.month
    print y, m
    if a.has_key(y) and a[y].has_key(m):
        a[y][m].append(b)
    elif not a.has_key(y):
        a[y] = {m:[b]}        
    elif a.has_key(y) and not a[y].has_key(m):
        a[y][m] = [b]
    return a

def tree_by_created(objects,timezone=None):
    items = copy(objects)
    items.insert(0,{})
    r = reduce(partial(archive_dict, timezone=timezone), items)
    return r

def _refresh_orders(parent_oid):
    dbconn = cherrypy.request.dbconn
    objects = object_children(parent_oid)
    count = 0
    objects = sort_by_order(parent_oid, objects)
    counter = 0
    objects_sort = []
    for obj in objects:
        objects_sort.append((counter,obj))
        counter = counter + 1
    count = 0
    objects_sort.sort()
    for obj_t in objects_sort:
        if obj_t[1].order[0].order:
            result = dbconn.execute(order_table.update(order_table.c.id==obj_t[1].order[0].id), order = count)
            result.close()
        else:
            result = dbconn.execute(order_table.insert(values={'oid':obj_t[1].oid,'parent_oid':parent_oid, 'order':count}))
            result.close()
        count = count + 1

def sort_by_order(parent_oid, objects):
    dbconn = cherrypy.request.dbconn    
    changed = False
    count = 0
    if isinstance(objects, list):
        for obj in objects:
            if hasattr(obj, 'order'):
                if not isinstance(obj.order, list):
                    changed = True
                    result = dbconn.execute(order_table.insert(values={'oid':obj.oid,'parent_oid':parent_oid, 'order':count}))
                    result.close()
            count = count + 1
        if changed:
            _refresh_orders(parent_oid)
            objects = object_children(parent_oid)
        order = map(lambda obj: int(obj.order[0].order), objects)
        li = sorted(zip(order, objects), key=operator.itemgetter(0))
        return map(operator.itemgetter(1),li)
    else:
        return objects

def _order_diff(list1, list2):
    diff_list = []
    for item1 in list1:
        for item2 in list2:
            if item1[1] == item2[1]:
                diff = int(item1[0]) - int(item2[0])
                diff_list.append((item1[1], diff))
    return diff_list

@cherrypy.expose
def change_order_many(parent_oid, object_oids):
    dbconn = cherrypy.request.dbconn
    #this is an ugly way to get the oid, and needs to have a better solution
    parent_oid = parent_oid.replace("!","")
    _refresh_orders(parent_oid)
    selected = get_object(oid=parent_oid)
    permissions = get_permissions(selected, get_session())
    if permissions['write']:
        new = object_oids.split(",")

        #create a list of tuples with a new order number, and oid
        new_order = []
        counter = 0
        for oid in new:
            new_order.append((int(counter),str(oid)))
            counter = counter+1

        #create a sorted list of tuples with the old order number, and oid
        old_order = []
        for oid in new:
            obj = get_object(oid)
            old_order.append((int(obj.order[0].order),str(obj.oid)))
            old_order.sort()

        #refresh the old order numbers to make sure there are not duplicates
        counter = 0
        old_order_reset = []
        for old in old_order:
            old_order_reset.append((int(counter),str(old[1])))
            counter = counter + 1

        #compare the two lists
        diff_list = _order_diff(new_order, old_order_reset)

        #save/update with the new order numbers
        for item in diff_list:
            obj = get_object(item[0])
            result = dbconn.execute(order_table.update(order_table.c.id == obj.order[0].id), \
                            order = obj.order[0].order + item[1])
            result.close()

@cherrypy.expose
def get_parent(oid):
    dbconn = cherrypy.request.dbconn
    selected = dbconn.execute(select([heirarchy_table], heirarchy_table.c.oid==oid)).fetchone()
    parent = get_object(selected['parent_oid'])
    return parent

@cherrypy.expose
def get_sibling(oid, steps):
    object = get_object(oid)
    parent = get_parent(oid)
    _refresh_orders(parent.oid)
    objects = sort_by_order(parent.oid, object_children(parent.oid))
    for _object in objects:
        if _object.oid == object.oid:
            try:
                if object.order[0].order+steps >= 0:
                    return objects[object.order[0].order+steps]
                else:
                    return False
            except:
                return False

def _object_date_compare(a,b):
    if a.created > b.created:
        return 1
    elif a.created == b.created:
        return 0
    elif a.created < b.created:
        return -1

def sort_by_created(objects):
    objects.sort(_object_date_compare)
    objects.reverse()
    return objects

def datetime_to_rfc822(the_time):
    return the_time.strftime("%a , %d %b %Y %H:%M:%S ") + "GMT"

@cherrypy.expose
def get_xml_feed(oid, format="rss2", max_number=10):
    cherrypy.response.headers['Content-Type'] = "text/xml"
    obj = get_object(oid)
    objects = sort_by_created(object_children(oid))[:max_number]
    t = Template(file=settings.root.core_views_path+"/rss2.tmpl",searchList=[locals()])
    return t.respond()

def get_permissions(obj, session):
    try:
        obj_user = {"owner":obj.owner[0].user, "group":obj.owner[0].group, "everyone":True}
    except:
        obj_user = {"owner":None, "group":None, "everyone":True}
    user = {"owner":session['user'], "group":session['group'], "everyone":True}
    if user["owner"] == "root":
        read, write = True, True
    else:
        read, write = False, False
        perms = _read_permissions(obj)
        for x in ["owner", "group", "everyone"]:
            read = True if perms[x]["read"] and obj_user[x] == user[x] else read
            write = True if perms[x]["write"] and obj_user[x] == user[x] else write
    return {"read":read, "write":write}

def _read_permissions(obj):
    rw = []
    for c in struct.unpack("3c",str(obj.permissions)):
        read = True if c == "6" or c == "4" else False
        write = True if c == "6" or c == "2" else False
        rw.append({"read":read, "write":write})
    return {"owner": rw[0], "group": rw[1], "everyone": rw[2]}

def template_perms(session, vid=None):
    if session['user'] == 'root':
        read = True
        write = True
    else:
        read = True
        write = False
    return {"read":read, "write":write}

def uri_perms(session, uri=None):
    if session['user'] == 'root':
        read = True
        write = True
    else:
        read = True
        write = False
    return {"read":read, "write":write}

def get_session():
    try:
        user = cherrypy.session.get('user')
        group = cherrypy.session.get('group')
    except:
        user = "everyone"
        group = "everyone"
    return {'user':user, 'group':group}

@cherrypy.expose
def login(username, password):
    if username == "root":
        if password == settings.root.root_password:
            cherrypy.session['user'] = "root"
            cherrypy.session['group'] = "root"
    else:    
        try:
            user = get_user(username)
            if password == user.password:
                cherrypy.session['user'] = user.id
                cherrypy.session['group'] = user.group
            else:
                cherrypy.session['user'] = "everyone"
                cherrypy.session['group'] = "everyone"
        except:
            cherrypy.session['user'] = "everyone"
            cherrypy.session['group'] = "everyone"

@cherrypy.expose
def logout():
    cherrypy.session['user'] = None
    cherrypy.session['group'] = None

def get_user(username):
    user = db.session.query(User());
    return user

@cherrypy.expose
def save_history(last_uri):
    cherrypy.session['last_uri'] = last_uri

def get_history():
    history = {"last_uri":cherrypy.session.get('last_uri')}
    return history

class NoUri(Exception):
    pass

def get_uri(uri):
    dbconn = cherrypy.request.dbconn
    result = dbconn.execute(select([template_rels_table], template_rels_table.c.location==uri))
    row = result.fetchone()
    result.close()
    if row:
        return TemplateRelation(row['location'],row['oid'],row['vid'],row['children'])
    else:
        raise NoUri()

@cherrypy.expose
def copy_uri(original, new):
    if new[0] == "/":
        new = new[1:]
    new = "-".join(new.split("/"))
    orig = get_uri(original)
    save_uri(new, orig.oid, orig.vid, orig.children)
    if orig.children:
	children = orig.children.split(',')
        for child in children:
            try:
       	        orig_child = get_uri(orig.location+"-"+child)
	    	new_child = new+"-"+child
	    	copy_uri(orig_child.location, new_child)
	    except:
	        pass

@cherrypy.expose
def save_uri(location, oid=None, vid=None, children=None):
    dbconn = cherrypy.request.dbconn    
    permissions = uri_perms(get_session())
    if permissions['write']:
        save_history(location)
        try:
            selected = get_uri(location)
            if not oid: oid = selected.oid
            if not vid: vid = selected.vid
            if not children: children = selected.children
        except:
            selected = None
        if selected:
            dbconn.execute(template_rels_table.update(template_rels_table.c.location==location),oid=oid, vid=vid, children=children)
        else:
            dbconn.execute(template_rels_table.insert(values={'location':location,'oid':oid,'vid':vid,'children':children}))

def read_uri(uri):
        args = uri.location.split("-")
        uri_list = []
        if args[0] == "r":
            args[0] = ""
            if len(args) == 1:
                uri_list.insert(0,"")
        else:
            uri_list.insert(0,"")

        for arg in args:
            uri_list.append(arg)
        return "/".join(uri_list)

@cherrypy.expose
def delete_uri(location):
    dbconn = cherrypy.request.dbconn    
    permissions = uri_perms(get_session())
    if permissions['write']:
        result = dbconn.execute(template_rels_table.delete(template_rels_table.c.location==location))
        result.close()

@cherrypy.expose
def manage_uri():
    dbconn = cherrypy.request.dbconn
    session = get_session()
    permissions = uri_perms(session)
    if permissions["write"]:
        rows = dbconn.execute(select([template_rels_table])).fetchall()
        uris = []
        for row in rows:
            uris.append(TemplateRelation(row['location'],row['oid'],row['vid'],row['children']))
        output = "<a href='/cmd/save_uri/r/%s/%s'> create root uri" % (settings.root.root_oid.replace('\"',''), settings.root.default_vid.replace('\"',''))
        for uri in uris:
            output = "".join([output,"<br/>%s <a href='/cmd/delete_uri/%s'>delete</a>" % (uri.location, uri.location)])
        return output
    else:
        if session["user"] == None:
            user = "everyone"
        else:
            user = session["user"]
        return "Sorry, %s is now allowed to manage URIs." % (user)

class HTMLHead:
    def __init__(self):
        self.title = ""
        self.js = []
        self.css = []
        self.meta = []
        self.link = []
        
    def add_tag(self, tag, src="", name="", media="screen", content="", rel="", type="", title=""):
        if tag == "script":
            if not content == "":
                self.js.append({"type":type, "content":content})
            else:
                self.js.append({"type":type, "src":src})

        elif tag == "style":
            self.css.append({"type":type, "media":media, "src":src})
        elif tag == "meta":
            self.meta.append({"name":name, "content":content})
        elif tag == "link":
            self.link.append({"rel":rel, "title":title, "type":type, "src":src})
        elif tag == "title":
            self.title = title
        else:
            pass
        
    def generate(self):
        output = ""
        try:
            new = "<title>%s</title>\n" % (self.title)
            output = "".join([output, new])
        except:
            pass
        for meta in self.meta:
            new = "<meta name='%s' content='%s' />\n" % (meta["name"], meta["content"])
            output = "".join([output, new])
        for css in self.css:
            new = "\t<style type='%s' media='%s'>@import url(%s);</style>\n" % (css["type"], css["media"], css["src"])
            output = "".join([output, new])
        for js in self.js:
            try:
                js["content"]
                new = "\t<script type='%s'>" % (js["type"])
                new = "".join([new, js["content"].replace("\n",""), "</script>\n"])
            except:
                new = "\t<script type='%s' src='%s'></script>\n" % (js["type"], js["src"])
            output = "".join([output, new])
        for link in self.link:
            new = "\t<link rel='%s' title='%s' href='%s' />\n" % (link["rel"], link["title"], link["src"])
            output = "".join([output, new])
        return output

def get_template(vid):
    dbconn = cherrypy.request.dbconn
    result = dbconn.execute(select([templates_table], templates_table.c.vid==vid))
    row = result.fetchone()
    result.close()
    if row:
        return Templates(row['name'],row['parent'],row['vid'],row['children'],row['source'])
    else:
        return None

@cherrypy.expose
def delete_template(vid):
    dbconn = cherrypy.request.dbconn
    permissions = template_perms(get_session())
    if permissions['write']:
        selected = get_template(vid)
        result = dbconn.execute(templates_table.delete(templates_table.c.vid == vid))       
        fs.templates.delete(selected.source)
        result.close()

def _tmpl_source_name(selected):
    source = ""
    selected.heirarchy = _tmpl_tree_path(selected.vid,heirarchy=[])
    
    if len(selected.heirarchy) >= 1:
        for parent in selected.heirarchy:
            sel_parent = get_template(parent)
            source += sel_parent.name+"_"
    source += selected.name+".tmpl"
    return source

def _tmpl_tree_path(vid, heirarchy=[]):
    selected = get_template(vid)
    if selected.parent:
        heirarchy.append(selected.parent)
        _tmpl_tree_path(selected.parent, heirarchy=heirarchy)
    return heirarchy

def _template_children(parent_vid):
    dbconn = cherrypy.request.dbconn
    results = dbconn.execute(select([templates_table], templates_table.c.parent==parent_vid)).fetchall()
    ret = []
    for row in results:
        ret.append(Templates(row['name'], row['parent'], row['vid'], row['children'], row['source']))
    return ret

@cherrypy.expose
def save_template(name=None, parent=None, vid=None, children=None):
    dbconn = cherrypy.request.dbconn
    if parent == "None": parent = None
    if children == "None": children = None
    permissions = template_perms(get_session())
    if permissions['write']:
        if vid:
            #update the existing row with new data
            selected = get_template(vid)
            if name != None:
                new_name = name
            else:
                new_name = selected.name
            source = selected.source
            result = dbconn.execute(templates_table.update(templates_table.c.vid==vid), name=name, parent=parent, children=children)
            result.close()
            #rename template file & update source in template_table
            selected = get_template(vid)
            new_source = _tmpl_source_name(selected)
            result = dbconn.execute(templates_table.update(templates_table.c.vid==vid), source=new_source)
            result.close()            
            fs.templates.rename(source, new_source)            
        else:
            #insert a new row into the table
            result = dbconn.execute(templates_table.insert(values={'name':name, 'parent':parent, 'children':children}))
            #update with the sourcename
            vid = result.last_inserted_ids()[0]
            result.close()
            selected = get_template(vid)
            new_source = _tmpl_source_name(selected)
            result = dbconn.execute(templates_table.update(templates_table.c.vid==vid), source = new_source)
            result.close()
            #create template file
            fs.templates.create(new_source)            

def search_list(object_oid=None, container=None, location=None, locations=None, cmds_array=None, kwargs=None):
    if kwargs:
        kwargs = kwargs
    if object_oid:
        object = get_object(object_oid)
        objects = object_children(object_oid)
    if location:
        location = location
    if cmds_array:
        cmds_array = cmds_array
    if container:
        container = container
    if locations:
        locations = locations
    return locals()

def render_template(source, data):
    data = data
    filename = source
    t = Template(file=filename,searchList=[data])
    return t.respond()

def compile_templates(location, kwargs):
    error = False
    children = []
    locations = []
    
    doc = get_uri(location)
    doc.template = get_template(doc.vid)
    doc.cmds_array = []
    doc.objects = []

    if doc.template.children != None:
        for child in doc.template.children.split(","):
            children.append(child)

    if len(children) >= 1:
        for child in children:
            locations.append(child)
            location_id = location+"-"+child
            try:
                container, cmds_array = compile_templates(location_id, kwargs)

            except NoUri:
                error = """Error: No URI found.<br/>
                        Please select this URI view,<br/>
                        and associate it with a template<br/>
                        or an object set.<br/>
                        Traceback: %s
                        """ % (traceback.format_exc(sys.stdout))
            except NotFound:
                error = """Error: No object found.<br/>
                        Please select this URI veiw,<br/>
                        and associate it with an object.<br/>
                        Traceback: %s
                        """ % (traceback.format_exc(sys.stdout))

            except AttributeError:
                error = """Error: No template found.<br/>
                        Please select this URI view,<br/>
                        and associate it with a template.<br/>
                        Traceback: %s
                        """ % (traceback.format_exc(sys.stdout))

            except:
                print sys.exc_info()[0]
                error = traceback.format_exc(sys.stdout)

            finally:
                if error:
                    t = Template("""<div id="$location_id">$error</div>""", searchList=[locals()])
                    container = t.respond()
                    cmds_array = "{id:'"+location_id+"', selected:false, type:'location'},"

            doc.objects.append(container)
            doc.cmds_array.append(cmds_array)

    else:
        for child in children:
            locations = child
            selected = get_uri(location+"-"+child)
            data = search_list(selected.oid, location=location, locations=locations, cmds_array=doc.cmds_array, kwargs=kwargs)
            doc.objects = render_template(settings.root.views_path+"/"+selected.template.source, data)
            cmds_array = render_template(settings.root.radial_menu_path+"/json_selectables.tmpl",data)

    data = search_list(doc.oid, doc.objects, location=location, locations=locations, cmds_array=doc.cmds_array, kwargs=kwargs)
    body = render_template(settings.root.views_path+"/"+doc.template.source, data)
    cmds_array = render_template(settings.root.radial_menu_path+"/json_selectables.tmpl", data)
    
    return body, cmds_array

def render_skeleton(location, location_st, kwargs):
    kwargs = kwargs
    location_st = location_st
    settings = Settings()
    site_name = settings.root.site_name
    session = get_session()
    user = session["user"]
    group = session["group"]
    body, cmds_array = compile_templates(location, kwargs)
    head = render_head(location, location_st, cmds_array)
    t = Template(file=settings.root.core_views_path+"/skeleton.tmpl", searchList=[locals()])
    return t.respond()

def walk_objects(selected, location_id=None):
    selected = selected
    try:
        selected.children = object_children(selected.oid)
    except:
        selected.children = None
    children = []
    if selected.children:
        for child in selected.children:
            object, container = (child, walk_objects(child, location_id))
            data = (object, container)
            children.append(data)
    t = Template(file=settings.root.radial_menu_path+"/walker_object.tmpl", searchList=[locals()])
    return t.respond()

def walk_templates(selected=None, parent_vid=None, location_id=None):
    sel = selected
    vid = parent_vid
    location_id = location_id
    if parent_vid:
        vid = parent_vid
    elif selected:
        vid = sel.vid
    tmpls = _template_children(vid)
    children = []
    if tmpls:
        for tmpl in tmpls:
            object, container = (tmpl, walk_templates(tmpl, location_id=location_id))
            data = (object, container)
            children.append(data)
    t = Template(file=settings.root.radial_menu_path+"/walker_template.tmpl", searchList=[locals()])
    return t.respond()

def list_uri(location, display="Form"):
    dbconn = cherrypy.request.dbconn    
    location_id = "-".join(location)
    results = dbconn.execute(select([template_rels_table])).fetchall()
    uris = []
    for r in results:
       uris.append(TemplateRelation(r['location'], r['oid'], r['vid'], r['children'])) 
    if display == "Form":
        t = Template(file=settings.root.radial_menu_path+"/list_uri.tmpl", searchList=[locals()])
    elif display == "Dropdown":
        t = Template(file=settings.root.radial_menu_path+"/list_uri_dropdown.tmpl", searchList=[locals()])
    return t.respond()

def render_head(location, location_st, cmds_array):
    settings = Settings()
    html_head = HTMLHead()
    html_head.add_tag("title",title=location_st)
    for stylesheet in settings.root.stylesheets:
        html_head.add_tag("style", stylesheet.replace('\"',''), type="text/css")
    for javascript in settings.root.javascript:
        html_head.add_tag("script", javascript.replace('\"',''), type="text/javascript")
    session = get_session()
    if session["user"] == "everyone":
        pass
    elif session["user"]:
        menu_array = "var ITEMS = ["
        menu_array = "".join([menu_array, cmds_array])
        menu_array = "".join([menu_array, "];"])
        html_head.add_tag("script", type="text/javascript", content=menu_array)
        menu_init = "/js/sparrow.init.js"
        html_head.add_tag("script", menu_init, type="text/javascript")
        html_head.add_tag("script", "/js/jquery.dimensions.js", type="text/javascript")
        html_head.add_tag("script", "/js/jquery.tooltip.js", type="text/javascript")
        html_head.add_tag("style", "/css/sparrow.menu.css", type="text/css")
        html_head.add_tag("style", "/css/jquery.tooltip.css", type="text/css")
    return html_head.generate()


def render(args, kwargs, location_st=None):
    settings = Settings()
    if len(args) >= 1:
        location = []
        for arg in args:
            location.append(arg)
        location_id = "-".join(location)
        try:
            uri = get_uri(location_id)
            return render_skeleton(location_id, location_st, kwargs)
        except NoUri:
            site_name = settings.root.site_name
            session = get_session()
            user = session["user"]
            group = session["group"]
            notfound = Template(file=settings.root.core_views_path+"/404.tmpl", searchList=[locals()])
            body = notfound.respond()
            cmds_array = ""
            head = render_head(location, location_st, cmds_array)
            t = Template(file=settings.root.core_views_path+"/skeleton.tmpl", searchList=[locals()])
            error = t.respond()
            cherrypy.request.error_page[404] = ErrorPage(body=error)
            raise HTTPError(404)
    else:
        location ="r"
        location_st = settings.root.site_name
        return render_skeleton(location, location_st, kwargs)

class ErrorPage():
    def __init__(self, status=None, body=None, message=None):
        self.body = body
        self.status = status
        self.message = message
        
    def __call__(self, **kwargs):
        return self.body

@cherrypy.expose
def manage_templates():
    dbconn = cherrypy.request.dbconn
    session = get_session()
    permissions = template_perms(session)
    if permissions["write"]:
        rows = dbconn.execute(select([templates_table])).fetchall()
        templates = []
        for row in rows:
            templates.append(Templates(row['name'],row['parent'],row['vid'],row['children'],row['source']))
        output = """
        Create Template 
        <form action="/cmd/save_template" method="post">
        Name:<input type="textfield" name="name" /> <br/>
        Parent:<select name="parent">
        <option value="None">None</option>
        """
        for template in templates:
            output = "".join([output,"<option value=%s>%s</option>" % (template.vid, template.source)])

        output = "".join([output, """
        </select><br/>
        <input type="submit" value="Create File, and Database Record"/>
        </form>
        """])

        output = "".join([output,"""
        Delete Template
        <form action="/cmd/delete_template" method="post">
        <select name="vid">
        <option value=''></option>
        """])
        
        for template in templates:
            output = "".join([output,"<option value='%s'>%s</option>" % (template.vid, template.source)])

        output = "".join([output,"""
        </select>
        <br/><input type="submit" value="Drop from Database, and Rename file to have *.bak" />
        </form>
        """])

        output = "".join([output,"""
        Change Template's Parent
        <form action="/cmd/save_template" method="post">
        <select name="vid">
        <option value=''></option>
        """])

        for template in templates:
            output = "".join([output,"<option value='%s'>%s</option>" % (template.vid, template.source)])

        output = "".join([output,"""
        </select>
        <select name="parent">
        <option value='None'>None</option>
        """])
        
        for template in templates:
            output = "".join([output,"<option value='%s'>%s</option>" % (template.parent, template.source)])

        output = "".join([output,"""
        </select>
        <br/><input type="submit" value="Alter Database, and Rename File" />
        </form>
        """])

        for template in templates:
            output = "".join([output,"""
            %s 
            <br/><form action='/cmd/save_template' method='POST'>
            <input type='hidden' name='vid' value='%s' />
            <input type='hidden' name='parent' value='%s' />
            name: <input type='textfield' name='name' value='%s' /><br/>
            children:<input type='textfield' name='children' value='%s'/>
            <input type='submit' value="Modify" /></form>""" % (template.source, template.vid, template.parent, template.name, template.children)])
        return output
    else:
        if session["user"] == None:
            user = "everyone"
        else:
            user = session["user"]
        return "Sorry, %s is now allowed to manage template files." % (user)

@cherrypy.expose
def add_heirarchy(object_id, the_heirarchy):
    dbconn = cherrypy.request.dbconn
    selected = get_object(object_id)
    permissions = permissions(selected, get_session())    
    if permissions['write']:
        result = db.conn.execute(heirarchy_table.insert(values={'oid':object_id,'parent_oid':the_heirarchy}))
        result.close()

def get_tree_objects(object_id, tree=None):
    try:
        parent = get_parent(object_id)
    except:
        parent = False
    if parent:
        tree.append(parent)
        return get_tree_objects(parent.oid, tree=tree)
    else:
        tree.reverse()
        return tree

@cherrypy.expose
def menu(object_id=None, location_id=None):
    if object_id:
        try:
            selected = get_object(object_id)
        except:
            selected = get_object(object_id)

        tmpl = settings.root.radial_menu_path+"/objects.tmpl"
        results = Template(file=tmpl, searchList=[locals()])
        return results.respond()
    elif location_id:
        try:
            location = get_uri(location_id)
        except:
            try:
                location = get_uri(location_id)
            except:
                location = TemplateRelation(location=location_id)
        root_object = get_object(oid="19")
        template_list = walk_templates(parent_vid=None, location_id=location_id)
        objects = walk_objects(root_object, location_id=location_id)
        tmpl = settings.root.radial_menu_path+"/views.tmpl"
        results = Template(file=tmpl, searchList=[locals()])
        return results.respond()
