"""
Sparrow, Settings Package
(c)Copyright 2007, Braydon Fuller
http://braydon.com

Sparrow is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Sparrow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from ConfigParser import *

class SiteSettings:
    def __init__(self, root_settings=None, object_settings=None, db_settings=None):
        self.root = root_settings
        self.object = object_settings
        self.database = db_settings

class RootSettings:
    def __init__(self, 
                 sparrow_path="", 
                 views_path="", 
                 root_password="", 
                 root_oid=0, 
                 site_name="", 
                 skeleton_template="", 
                 missing_template="", 
                 javascript=[], 
                 stylesheets=[]):
        self.sparrow_path = sparrow_path
        self.skeleton_template = skeleton_template
        self.missing_template = missing_template
        self.views_path = views_path
        self.root_password = root_password
        self.root_oid = root_oid
        self.site_name = site_name
        self.javascript = javascript

class ObjectSettings:
    def __init__(self, base_path="", images="", video="", audio="", html=""):
        self.base_path = base_path
        self.images = images
        self.video = video
        self.audio = audio
        self.html = html

class DatabaseSettings:
    def __init__(self, user="", password="", host="", port="", database_name="", database=""):
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.database_name = database_name
        self.database = database

def get_settings():
    config = ConfigParser()
    config.read('/home/braydon/www/mochilla/config/server.conf')
    root_settings = RootSettings()
    object_settings = ObjectSettings()
    db_settings = DatabaseSettings()
    root_settings.core_views_path = config.get("root","core_views_path").replace('\"','')
    root_settings.sparrow_path = config.get("root","sparrow_path").replace('\"','')
    root_settings.views_path = config.get("root","views_path").replace('\"','')
    root_settings.root_password = config.get("root","root_password").replace('\"','')
    root_settings.base_url = config.get("root","base_url").replace('\"','')
    root_settings.root_oid = config.get("root","root_oid")
    root_settings.default_vid = config.get("root","default_vid")
    root_settings.site_name = config.get("root","site_name").replace('\"','')
    root_settings.stylesheets = str(config.get("root","stylesheets")).split(",")
    root_settings.javascript = str(config.get("root","javascript")).split(",")
    root_settings.radial_menu_path = config.get("root","radial_menu_path").replace('\"','')
    root_settings.tmp_path = config.get("root","tmp_path").replace('\"','')
    object_settings.base_path = config.get("objects","base_path").replace('\"','')
    object_settings.images = config.get("objects","images").replace('\"','')
    object_settings.video = config.get("objects","video").replace('\"','')
    object_settings.audio = config.get("objects","audio").replace('\"','')
    object_settings.html = config.get("objects","html").replace('\"','')
    object_settings.fonts = config.get("objects","fonts").replace('\"','')
    object_settings.text_to_image = config.get("objects","text_to_image").replace('\"','')
    db_settings.user = config.get("database","user").replace('\"','')
    db_settings.password = config.get("database","password").replace('\"','')
    db_settings.host = config.get("database","host").replace('\"','')
    db_settings.port = config.get("database","port").replace('\"','')
    db_settings.database_name = config.get("database","database_name").replace('\"','')
    db_settings.database = config.get("database","database").replace('\"','')
    return SiteSettings(root_settings, object_settings, db_settings)

class Settings(object):
    settings = get_settings()
    def __init__(self):
        self.root = Settings.settings.root
        self.object = Settings.settings.object
        self.database = Settings.settings.database
